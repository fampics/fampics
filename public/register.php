<?php

include '../src/common.inc';
include '../src/Register.php';

$message = array();

if (isset($_POST['email'])) {
	
	$url = $_SITE_URL_;
	try{
		$regi = Register::registration($_POST['username'],$_POST['email'],$_POST['password']);
		$regi->submit($auth,$url);
		try {
			$template = $GLOBALS['twig']->load('emails/NewUserConfirmationEmail.html');
			$message = $template->render(array("sitename" => $_SITENAME_, "url" => $regi->url));
			$mail->setFrom('from@example.com',$_SITENAME_);
			$mail->addAddress($_POST['email']);
			$mail->isHTML(true);
			$mail->Subject = "Email confirmation for " . $_SITENAME_ ;
			$mail->Body = $message;
			$mail->send();
			$message['email'] = array("text" => "Registration succesful, please check your email for a confirmation email.", "state" => "valid");
		}
		catch (Exception $e) {
			$message['email'] = array("text" => $e, "state" => "invalid");
		}
	}
	catch (\Delight\Auth\InvalidEmailException $e) {
		$message['email'] = array("text" => "Invalid Email", "state" => "invalid");
	}
	catch (\Delight\Auth\InvalidPasswordException $e) {
		$message['password'] = array("text" => "Invalid Password", "state" => "invalid");
	}
	catch (\Delight\Auth\UserAlreadyExistsException $e) {
		$message['email'] = array("text" => "That email is already in use", "state" => "invalid");
	}
	catch (\Delight\Auth\TooManyRequestsException $e) {
		$message['email'] = array("text" => "Too many Requests", "state" => "invalid");
	}
	catch (\Delight\Auth\DuplicateUsernameException $e) {
		$message['username'] = array("text" => "That username is already used", "state" => "invalid");
	}
	catch (MissingUserNameException $e) {
		$message['username'] = array("text" => "Please enter a username", "state" => "invalid");
	}

}

$template = $twig->load('register.html');
echo $template->render(array("sitename" => $_SITENAME_, "siteurl" => $_SITE_URL_, "HIDE_NAVBAR" => true, "message" => $message));
