<?php
$TEMPLATE_DISABLE = true;
include '../inc/common.inc';

$stmt = $conn->prepare("UPDATE notifications SET beenread = 1 WHERE user= ?");
$stmt->bind_param('i', $user['id']);
$stmt->execute();

if ($stmt->affected_rows > 0) {
	echo "True";
}
else {
	echo "Error: " . $conn->error;
}
?>
