<?php
$TEMPLATE_DISABLE = true;
include_once '../../src/common.inc';
include '../../src/imageConverts.php';
include '../../src/database.php';

$db = new Database();

switch ($_POST['method']) {
	case "approveUser":
		$stmt = $conn->prepare("INSERT INTO users_meta (id, userid, tag, value, timestamp) VALUES (NULL, ?, 'UserApproved', 'true', CURRENT_TIMESTAMP) ");
		$stmt->bind_param('i', $_POST['userID']);
		$stmt->execute();
		break;
	case "getUserRoles":
		$roles_auth = $auth->admin()->getRolesForUserById($_POST['userID']);
		$roles = array();
		foreach ($roles_auth as $r) {
			$roles[] = $r;
		}
		echo json_encode($roles);
		break;
	case "setUserRoles":
		$auth->admin()->removeRoleForUserById($_POST['userID'], \Delight\Auth\Role::EDITOR);
		$auth->admin()->removeRoleForUserById($_POST['userID'], \Delight\Auth\Role::MODERATOR);
		$auth->admin()->removeRoleForUserById($_POST['userID'], \Delight\Auth\Role::PUBLISHER);
		$auth->admin()->removeRoleForUserById($_POST['userID'], \Delight\Auth\Role::ADMIN);
		if ($auth->hasRole(\Delight\Auth\Role::SUPER_ADMIN))
		{
			$auth->admin()->removeRoleForUserById($_POST['userID'], \Delight\Auth\Role::SUPER_ADMIN);
		}
		if ($_POST['editor'] == "true")
		{
			$auth->admin()->addRoleForUserById($_POST['userID'], \Delight\Auth\Role::EDITOR);
		}
		if ($_POST['moderator'] == "true")
		{
			$auth->admin()->addRoleForUserById($_POST['userID'], \Delight\Auth\Role::MODERATOR);
		}
		if ($_POST['publisher'] == "true")
		{
			$auth->admin()->addRoleForUserById($_POST['userID'], \Delight\Auth\Role::PUBLISHER);
		}
		if ($_POST['admin'] == "true")
		{
			$auth->admin()->addRoleForUserById($_POST['userID'], \Delight\Auth\Role::ADMIN);
		}
		if ($_POST['super_admin'] == "true" && $auth->hasRole(\Delight\Auth\Role::SUPER_ADMIN))
		{
			$auth->admin()->addRoleForUserById($_POST['userID'], \Delight\Auth\Role::SUPER_ADMIN);
		}
		break;
	case "changeUserPassword":
		$auth->admin()->changePasswordForUserById($_POST['userID'], $_POST['password']);
		break;
	case "BanUser":
		$stmt = $conn->prepare("INSERT INTO users_meta (id, userid, tag, value, timestamp) VALUES (NULL, ?, 'UserBanned', ?, CURRENT_TIMESTAMP) ");
		$stmt->bind_param('is', $_POST['UserID'], $_POST['mesg']);
		$stmt->execute();
		break;
	case "UnBanUser":
		$stmt = $conn->prepare("DELETE FROM users_meta WHERE tag = 'UserBanned' AND userid = ?");
		$stmt->bind_param('i', $_POST['UserID']);
		$stmt->execute();
		break;
	case "generateThumb":
		$picID = $_POST['picid'];
		// 1. [X] Pull image path from DB.
		$picDetails = $db->getPicDetailsFromId($picID);
		//2. [X] Break filepath into components
		$pic_filename = basename($picDetails['filepath']);
		$pic_path = $_SERVER['DOCUMENT_ROOT'] . '/../' . $_PIC_BASE_DIR_ . dirname($picDetails['filepath']);
		$source_full_path = $pic_path . '/' . $pic_filename;
		$thumb_full_path = $_SERVER['DOCUMENT_ROOT'] . '/../' . $_THUMB_DIR_ . dirname($picDetails['filepath']) . '/' . $pic_filename;
		//3. [X] make reduced size copy of image with Image Magick
		$image = new imageConverter();
		$response = $image->generateThumb($source_full_path, $thumb_full_path);
		//4. [X] save image in thumbs folder
		//5. [X] Add thumb into the thumbs folder
		//6. [ ] set image record to use new thumb
		$thumbpath = $picDetails['filepath'];
		if (!$response->hasFailed()){
			$db->addThumb($thumbpath, $picID);
		}
		break;
	default:
		break;
}
?>
