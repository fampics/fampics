<?php
$TEMPLATE_DISABLE = true;
$TEMPLATE_ALTERNATE = '../static';
include '../inc/common.inc';
include '../inc/Parsedown_fampics.php';
include '../inc/notifier.php';
$debug = array();
$Parsedown = new Parsedown_fampics();

if (isset($_POST['commentid'])){
	$parent = $_POST['commentid'];
}
else {
	$parent = null;
}

$parsed = $Parsedown->text($_POST['comment']);

$userid = $auth->getUserId() ;

$stmt = $conn->prepare("INSERT INTO comments (id, pic, comment, timestamp, userid, parent) Values (Null, '?', '?', CURRENT_TIMESTAMP, '?', '?')");
$stmt->bind_param("isii", $_POST['pic'], $_POST['comment'], $userid, $parent);
$stmt->execute();

if ($stmt->affected_rows > 0) {
	
	scanComment($_POST['comment'], $_POST['pic'], $auth, $conn, $parent);
	$return = array(
		"status" => "Sucess",
		"parsed" => $parsed,
		"DEBUG" => $GLOBALS['debug']
	);
}
else {
	$return = array(
		"status" => "Failed",
		"status-mesg" => $conn->error,
		"SQL" => $sql,
		"parsed" => $parsed,
		"DEBUG" => $GLOBALS['debug']
	);
}

echo json_encode($return);
?>
