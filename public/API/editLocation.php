<?php
$TEMPLATE_DISABLE = true;
include '../inc/common.inc';

if (!canEditImageLocation($auth)) {
	header("Location: " . $_SITE_URL_ . "force403");
}

$stmt = $conn->prepare("UPDATE picture SET location = ? WHERE picture.id = ?");
$stmt->bind_param("ii", $_POST['locationid'], $_POST['pictureid']);
$stmt->execute

if ($stmt->affected_rows > 0) {
	echo "True";
}
else {
	echo "Error: " . $conn->error;
}
?>
