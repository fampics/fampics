<?php

include '../src/common.inc';
include '../src/login.php';

$message = array();
if (!empty($_POST['email'])){

	$cb = isset($_POST['keepalive']) ? $_POST['keepalive'] : null;
	$login_obj = Login::setup($_POST['email'], $_POST['password'], $cb, $_GET['goto']);

	$message['email'] = $login_obj->login($auth);

}

$template = $twig->load('login.html');
$template->display(array("sitename" => $_SITENAME_, "siteurl" => $_SITE_URL_, "referer" => $_GET['goto'], "HIDE_NAVBAR" => true, "message" => $message));


?>
