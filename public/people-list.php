<?php
include '../src/common.inc';

if (!$auth->isLoggedIn() and $_LOGIN_REQUIRED_) {
	header('Location: '.$_SITE_URL_.'login?goto=people');
}

$stmt = $conn->prepare("
	SELECT people.l_name, people.f_name
	FROM people
	ORDER BY l_name, f_name
	");
$stmt->execute();
$result = $stmt->get_result();

$rows = array();

while ($row = mysqli_fetch_assoc($result)) {
	$rows[] = $row;
}
$template = $twig->load('people-list.html');
echo $template->render(array("sitename" => $_SITENAME_, "siteurl" => $_SITE_URL_, "basepath" => $_PIC_BASE_DIR_, "thumbbasepath" => $_THUMB_DIR_, "names" => $rows, "user" => $user));
?>
