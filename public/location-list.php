<?php

include '../src/common.inc';

if (!$auth->isLoggedin() and $_LOGIN_REQUIRED_) {
	header('Location: ' . $_SITE_URL_ . 'login?goto=location');
}

$stmt = $conn->prepare("SELECT * FROM locations");
$stmt->execute();
$result = $stmt->get_result();


$locations = array();

while ($location = mysqli_fetch_assoc($result)) {
	$locations[] = $location;
}

$location_tree = array();


// add the top level locations to the tree.
for ($i = 0; $i < count($locations); $i++){
	$cur = $locations[$i];
	if ($cur['parent'] == 0){
		$location_tree[] = $cur;
	}
}

// Add the second layer of locations
for ($i = 0; $i < count($location_tree); $i++){
	for ($j = 0; $j < count($locations); $j++){
		if ($location_tree[$i]['id'] == $locations[$j]['parent'] and $location_tree[$i]['id'] != 0){
			$location_tree[$i]['children'][] = $locations[$j];
		}
	}
}

// Add the third layer of locations.
for ($i = 0; $i < count($location_tree); $i++){
	if (isset($location_tree[$i]['children'])){
		for ($j = 0; $j < count($location_tree[$i]['children']); $j++){
			for ($k = 0; $k < count($locations); $k++) {
				if ($locations[$k]['parent'] == $location_tree[$i]['children'][$j]['id']){
					$location_tree[$i]['children'][$j]['children'][] = $locations[$k];
				}
			}
		}
	}
}



$template = $twig->load('location-list.html');
echo $template->render(array("sitename" => $_SITENAME_, "siteurl" => $_SITE_URL_, "basepath" => $_PIC_BASE_DIR_, "thumbbasepath" => $_THUMB_DIR_, "user" => $user, "location_tree" => $location_tree, "mapbox_token" => $_MAPBOX_TOKEN_, "locations" => $locations));

?>
