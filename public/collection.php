<?php
include '../src/common.inc';
include '../src/Parsedown_fampics.php';

$Parsedown = new Parsedown_fampics();

if (!$auth->isLoggedIn() and $_LOGIN_REQUIRED_) {

	if (isset($_GET['pic'])) {
		header('Location: '.$_SITE_URL_.'login?goto=album_'.$_GET['id']."/page_".$_GET['pic']);
	}
	else{
		header('Location: '.$_SITE_URL_.'login?goto=album_'.$_GET['id']);
	}
}

$stmt = $conn->prepare("SELECT * FROM collection WHERE id = ?");
$stmt->bind_param('i', $_GET['id']);
$stmt->execute();
$result = $stmt->get_result();
$collection = mysqli_fetch_assoc($result);

if ($collection['type'] == 2) { // The collection is of type "Album"

	if (isset($_GET['pic'])) {
		$picnum = $_GET['pic'];
	}
	else {
		$picnum = "0";
	}
	$stmt = $conn->prepare("SELECT * FROM picture WHERE collection = ? ORDER BY picture.pagenum LIMIT ? ,1");
	$stmt->bind_param('ii', $collection['id'], $picnum);
	$stmt->execute();
	$result = $stmt->get_result();
	$pic = mysqli_fetch_assoc($result);

	$stmt = $conn->prepare("
		SELECT zoomins.*,
		picture.*,
		l1.name AS locationname,
		l2.name AS parent1,
		l3.name AS parent2
		FROM zoomins
		LEFT JOIN picture ON zoomins.imgid = picture.id
		LEFT JOIN locations AS l1 ON picture.location = l1.id
		LEFT JOIN locations AS l2 ON l1.parent = l2.id
		LEFT JOIN locations AS l3 ON l2.parent = l3.id
		WHERE pageid = ?
		");
	$stmt->bind_param('i', $pic['id']);
	$stmt->execute();
	$result = $stmt->get_result();
	$click_zones = array();
	while($click = mysqli_fetch_assoc($result)) {
		// Load tags
		$stmt = $conn->prepare("
			SELECT * FROM tag_instance
			LEFT JOIN tags ON tag_instance.tagid = tags.id
			WHERE tag_instance.pic = ?
			");
		$stmt->bind_param('i', $click['imgid']);
		$stmt->execute();
		$result2 = $stmt->get_result();
		$tags = array();
		while ($tag = mysqli_fetch_assoc($result2)) {
			$tags[] = $tag;
		}
		if (!empty($tags)) { 
			$click['tags'] = $tags;
		}
		// load facetags
		$stmt = $conn->prepare("
			SELECT tags.name, facetag.*, people.f_name, people.l_name FROM facetag
			LEFT JOIN tags ON facetag.person = tags.id
			LEFT JOIN people ON tags.id = people.tagid
			WHERE facetag.img = ?
			");
		$stmt->bind_param('i', $click['imgid']);
		$stmt->execute();
		$result3 = $stmt->get_result();
		$faces = array();
		while ($face = mysqli_fetch_assoc($result3)) {
			$faces[] = $face;
		}
		if(!empty($faces)) {
			$click['faces'] = $faces;
		}
		
		// Load comments
		$stmt = $conn->prepare("
			SELECT comments.*,
			users.email, users.username FROM comments
			LEFT JOIN users ON comments.userid = users.id
			WHERE pic = ?
			ORDER BY comments.timestamp
			");
		$stmt->bind_param('i', $click['imgid']);
		$stmt->execute();
		$results4 = $stmt->get_result();
		$comments = array();
		while ($comment = mysqli_fetch_assoc($results4)) {
			$parsed = $Parsedown->text($comment['comment']);
			$comment['comment'] = $parsed;
			$comments[] = $comment;
		}
		if(!empty($comments)) {
			$click['comments'] = $comments;
		}
		
		// Load it all into the array
		$click_zones[] = $click;
	}


	$template = $twig->load('viewers/album.html');
	echo $template->render(array("collection" => $collection, "sitename" => $_SITENAME_, "siteurl" => $_SITE_URL_, "basepath" => $_PIC_BASE_DIR_, "pic" => $pic, "picnum" => $picnum, "clickzones" => $click_zones, "user" => $user));
}
else {
	$stmt = $conn->prepare("
		SELECT picture.*,
		thumbs.thumbpath,
		l1.name AS locationname,
		l2.name AS parent1,
		l3.name AS parent2
		FROM picture
		LEFT JOIN thumbs ON picture.thumbid = thumbs.id
		LEFT JOIN locations AS l1 ON picture.location = l1.id
		LEFT JOIN locations AS l2 ON l1.parent = l2.id
		LEFT JOIN locations AS l3 ON l2.parent = l3.id
		WHERE picture.collection = ?
		");
	$stmt->bind_param('i', $collection['id']);
	$stmt->execute();
	$result = $stmt->get_result();
$pictures = array();
while($row = mysqli_fetch_assoc($result)) {
	$stmt = $conn->prepare("
		SELECT tag_instance.*,
		tags.*
		FROM tag_instance
		LEFT JOIN tags ON tag_instance.tagid = tags.id
		WHERE tag_instance.pic = ?
		");
	$stmt->bind_param('i', $row['id']);
	$stmt->execute();
	$result2 = $stmt->get_result();
	$tags = array();
	while($tag = mysqli_fetch_assoc($result2)) {
		$tags[] = $tag;
	}
	if (!empty($tags)) {
		$row['tags'] = $tags;
	}
	$stmt = $conn->prepare("
		SELECT tags.name, facetag.*, people.f_name, people.l_name FROM facetag
		LEFT JOIN tags ON facetag.person = tags.id
		LEFT JOIN people ON tags.id = people.tagid
		WHERE facetag.img = ?
		");
	$stmt->bind_param('i', $row['id']);
	$stmt->execute();
	$result3 = $stmt->get_result();
	$faces = array();
	while($face = mysqli_fetch_assoc($result3)){
		$faces[] = $face;
	}
	if(!empty($faces)){
		$row['faces'] = $faces;
	}

	$stmt = $conn->prepare("
		SELECT comments.*,
		users.email, users.username FROM comments
		LEFT JOIN users ON comments.userid = users.id
		WHERE pic = ?
		ORDER BY comments.timestamp
		");
	$stmt->bind_param('i', $row['id']);
	$stmt->execute();
	$result4 = $stmt->get_result();
	$comments = array();
	while($comment = mysqli_fetch_assoc($result4)) {
		$parsed = $Parsedown->text($comment['comment']);
		$comment['comment'] = $parsed;
		$comments[] = $comment;
	}
	if (!empty($comments)) {
		$row['comments'] = $comments;
	}

	$pictures[] = $row;

}

$template = $twig->load('collection.html');
echo $template->render(array("collection" => $collection, "sitename" => $_SITENAME_, "pictures" => $pictures, "basepath" => $_PIC_BASE_DIR_, "siteurl" => $_SITE_URL_, "thumbbasepath" => $_THUMB_DIR_, "user" => $user));
}

?>
