<?php
include '../src/common.inc';
include '../src/Parsedown_fampics.php';

$Parsedown = new Parsedown_fampics();

if (!$auth->isLoggedIn() and $_LOGIN_REQUIRED_) {
	header('Location: '.$_SITE_URL_.'login?goto=people/'.$_GET['l_name'].'/'.$_GET['f_name']);
}

$stmt = $conn->prepare("SELECT * FROM people WHERE people.l_name = ? AND people.f_name = ?");
$stmt->bind_param('ss', $_GET['l_name'], $_GET['f_name']);
$stmt->execute();
$result = $stmt->get_result();


$rows = array();

while($row = mysqli_fetch_assoc($result)) {
	$rows[] = $row;
}

$bio = $Parsedown->text($rows[0]['bio']);

$stmt = $conn->prepare("SELECT * FROM facetags_person WHERE personid = ?");
$stmt->bind_param("i", $rows[0]['id']);
$stmt->execute();
$result = $stmt->get_result();
$pics = array();
while($pic = mysqli_fetch_assoc($result)) {
	$pics[] = $pic;
}

$template = $twig->load('person.html');
echo $template->render(array("person" => $rows[0], "sitename" => $_SITENAME_, "siteurl" => $_SITE_URL_, "basepath" => $_PIC_BASE_DIR_, "thumbbasepath" => $_THUMB_DIR_, "pics" => $pics, "bio" => $bio, 'user' => $user));
?>
