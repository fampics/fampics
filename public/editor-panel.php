<?php

include '../src/common.inc';

if (!$auth->isLoggedIn()) {
	header('Location: '.$_SITE_URL_.'login?goto=editor');
}

if (!canOpenEditorPanel($auth)) {
	header('Location: '.$_SITE_URL_);
}

$template = $twig->load('editor-panel.html');
echo $template->render(array("sitename" => $_SITENAME_, "siteurl" => $_SITE_URL_, "user" => $user));
?>
