<?php
include '../src/common.inc';

if (!$auth->isLoggedIn()) {
	header('Location: '.$_SITE_URL_.'login?goto=settings');
}

$message=array();

if (isset($_POST['mode'])) {
	switch ($_POST['mode']) {
	case "email":
		try{
			$url = $_SITE_URL_;
			$auth->changeEmail($_POST['email'], function ($selector, $token) use (&$url) {
				$url .= "verify_email?selector=" . \urlencode($selector) . "&token=" . \urlencode($token);
			});
			try {
				$template = $twig-load("emails/ChangeEmail.html");
				$message = $template->render(array("url" => $url));
				$mail->setFrom('from@example.com','The webserver');
				$mail->addAddress($_POST['email']);
				$mail->isHTML(true);
				$mail->Subject = 'Change of Email Request';
				$mail->Body = $message;
				$mail->send();
				$message['email'] = array("text" => "Email Updated", "state" => "valid", "email" => $_POST['email']);
			}
			catch (Exception $e) {
				$message['email'] = array("text" => $e, "state" => "invalid", "value" => $_POST['email']);
			}
		}
		catch (\Deligh\Auth\InvalidEmailException $e) {
			$message['email'] = array("text" => "Invalid Email", "state" => "invalid", "email" => $_POST['email']);
		}
		catch (\Delight\Aurh\UserAlreadyExistsException $e) {
			$message['email'] = array("text" => "Email in use by another user", "state" => "invalid", "email" => $_POST['email']);
		}
		catch (\Delight\Auth\EmailNotVerifiedException $e) {
			$message['email'] = array("text" => "Email not verified", "state" => "invalid", "email" => $_POST['email']);
		}
		catch (\Delight\Auth\NotLoggedInException $e) {
			$message['email'] = array("text" => "You are not logged in", "state" => "invalid", "email" => $_POST['email']);
		}
		catch (\Delight\Auth\TooManyRequestsException $e) {
			$message['email'] = array("text" => "Too many requests", "state" => "invalid", "email" => $_POST['email']);
		}
		break;
	case "password":
		try {
			$auth->changePassword($_POST['old_pass'], $_POST['new_pass']);
			$message['new_pass'] = array("text" => "Password updated.", "state" => "valid");
		}
		catch (\Delight\Auth\NotLoggedInException $e) {
		}
		catch (\Delight\Auth\InvalidPasswordException $e) {
		}
		catch (\Delight\Auth\TooManyRequestsException $e) {
			$message['new_pass'] = array("text" => "Too many requests", "state" => "invalid");
		}
		break;
	}

}


$template = $twig->load('user-settings.html');
echo $template->render(array("sitename" => $_SITENAME_, "siteurl" => $_SITE_URL_, "basepath" => $_PIC_BASE_DIR_, "thumbbasepath" => $_THUMB_DIR_, "user" => $user, "message" => $message));


?>

