<?php

include '../src/common.inc';

if (!$auth->isLoggedIn() and $_LOGIN_REQUIRED_) {
	header('Location: '.$_SITEURL_.'login?goto=.');
}

$stmt = $conn->prepare("SELECT * FROM collection WHERE collection.display = 1");
$stmt->execute();
$result = $stmt->get_result();

$rows = array();

while($row = mysqli_fetch_assoc($result)) {
	if ($row["type"] != 6){
                $rows[] = $row;
	}
}

$template = $twig->load('index.html');
echo $template->render(array("collections" => $rows, "sitename" => $_SITENAME_, "siteurl" => $_SITE_URL_, "user" => $user));
//echo $template->render($row);


?>
