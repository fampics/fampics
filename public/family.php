<?php
include '../src/common.inc';

if (!$auth->isLoggedIn() and $_LOGIN_REQUIRED_) {
	header('Location: '.$_SITE_URL_.'login?goto=people/'.$_GET['l_name']);
}

$stmt = $conn->prepare("SELECT * FROM people WHERE people.l_name = ?");
$stmt->bind_param('s', $_GET['l_name']);
$stmt->execute();
$result = $stmt->get_result();


$rows = array();

while($row = mysqli_fetch_assoc($result)) {
	$rows[] = $row;
}

$template = $twig->load('family.html');
echo $template->render(array("people" => $rows, "sitename" => $_SITENAME_, "siteurl" => $_SITE_URL_, "basepath" => $_PIC_BASE_DIR_, "thumbbasepath" => $_THUMB_DIR_, "user" => $user));
?>
