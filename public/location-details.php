<?php

include '../src/common.inc';
include '../src/Parsedown_fampics.php';

$Parsedown = new Parsedown_fampics();

if (!$auth->isLoggedIn() and $_LOGIN_REQUIRED_) {
	header('Location: ' . $_SITE_URL_ . 'login?goto=location/' . $_GET['path']);
}

$path = array_reverse(explode('/', $_GET['path']));

if (isset($path[2])) {
	// there are two parent locations
	$stmt = $conn->prepare("
		SELECT 
		locations.id,
		locations.name,
		locations.lat,
		locations.lon,
		locations.zoom,
		locations.geometry,
		l2.name AS parent1,
		l3.name AS parent2,
		locations.bio
		FROM locations
		LEFT JOIN locations AS l2 ON locations.parent = l2.id
		LEFT JOIN locations AS l3 ON l2.parent = l3.id
		WHERE locations.name = ?
		AND l2.name = ?
		AND l3.name = ?
		");
	$stmt->bind_param('sss', $path[0], $path[1], $path[2]);
}
elseif (isset($path[1])) {
	// There is one parrent location
	
	$stmt = $conn->prepare("
		SELECT
		locations.id,
		locations.name,
		locations.lat,
		locations.lon,
		locations.zoom,
		locations.geometry,
		l2.name AS parent1,
		locations.bio
		FROM locations
		LEFT JOIN locations AS l2 ON locations.parent = l2.id
		WHERE locations.name = ?
		AND l2.name = ?
	");
	$stmt->bind_param("ss", $path[0], $path[1]);
}
else {
	// There are no parent locations
	$stmt = $conn->prepare("
		SELECT
		locations.id,
		locations.name,
		locations.lat,
		locations.lon,
		locations.zoom,
		locations.geometry,
		locations.bio
		FROM locations
		WHERE locations.name = ?
		AND locations.parent = 0		
	");
	$stmt->bind_param("s", $path[0]);
}

$stmt->execute();
$result = $stmt->get_result();
//$result = mysqli_query($conn, $sql);
$location = mysqli_fetch_assoc($result);

$stmt = $conn->prepare("
	SELECT picture.*, thumbs.thumbpath FROM picture
	LEFT JOIN thumbs ON picture.thumbid = thumbs.id
	WHERE location = ?
	");
$stmt->bind_param("i", $location['id']);
$stmt->execute();
$result2 = $stmt->get_result();

$pics = array();
while($pic = mysqli_fetch_assoc($result2)) {
	$pics[] = $pic;
}

$bio = $Parsedown->text($location['bio']);

$template = $twig->load('location-details.html');
echo $template->render(array("sitename" => $_SITENAME_, "siteurl" => $_SITE_URL_, "basepath" => $_PIC_BASE_DIR_, "thumbbasepath" => $_THUMB_DIR_, "location" => $location, "pics" => $pics, "bio" => $bio, "mapbox_token" => $_MAPBOX_TOKEN_, "user" => $user));


?>
