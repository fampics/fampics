<?php
include '../src/common.inc';
include '../src//Parsedown_fampics.php';

$Parsedown = new Parsedown_fampics();

if (!$auth->isLoggedIn() and $_LOGIN_REQUIRED_) {
	header('Location: '.$_SITE_URL_.'login?goto=image/'.$_GET['pic']);
}

$stmt = $conn->prepare("
	SELECT picture.*,
	collection.name AS collection_name,
	collection.type AS collection_type,
	collection.id AS collection_id,
	collection.`Linked album` AS true_collection,
	c2.name AS true_name,
	locations.name AS locationname,
	locations.lat, locations.lon, locations.geometry,
	p1.name AS loc_parent_1,
	p2.name AS loc_parent_2,
	p3.name AS loc_parent_3
	FROM picture
	LEFT JOIN collection ON picture.collection = collection.id
	LEFT JOIN collection AS c2 ON collection.`Linked album`=c2.id
	LEFT JOIN locations ON picture.location = locations.id
	LEFT JOIN locations AS p1 ON locations.parent = p1.id
	LEFT JOIN locations AS p2 ON p1.parent = p2.id
	LEFT JOIN locations AS p3 ON p2.parent = p3.id
	WHERE picture.id = ?
	");
$stmt->bind_param("i", $_GET['pic']);
$stmt->execute();
$result = $stmt->get_result();

$row = mysqli_fetch_assoc($result);

$stmt = $conn->prepare("
	SELECT tags.name FROM tag_instance
	LEFT JOIN tags ON tag_instance.tagid = tags.id
	WHERE tag_instance.pic = ?
	");
$stmt->bind_param('i', $row['id']);
$stmt->execute();
$result2 = $stmt->get_result();

$tags = array();

while($tag = mysqli_fetch_assoc($result2)) {
	$tags[] = $tag;
}

if(!empty($tags)) {
	$row['tags'] = $tags;
}

$stmt = $conn->prepare("
	SELECT tags.name, facetag.*, people.f_name, people.l_name FROM facetag
	LEFT JOIN tags ON facetag.person = tags.id
	LEFT JOIN people ON tags.id = people.tagid
	WHERE facetag.img = ?
	");
$stmt->bind_param("i", $row['id']);
$stmt->execute();

$result3 = $stmt->get_result();
$faces = array();

while($face = mysqli_fetch_assoc($result3)) {
	$faces[] = $face;
}

if(!empty($faces)) {
	$row['faces'] = $faces;
}

$stmt = $conn->prepare("
	SELECT p2.pagenum FROM picture
	LEFT JOIN zoomins ON picture.id = zoomins.imgid
	LEFT JOIN picture AS p2 ON zoomins.pageid = p2.id
	WHERE picture.id = ?
	");
$stmt->bind_param("i", $row['id']);
$stmt->execute();

$result4 = $stmt->get_result();
$page_num = mysqli_fetch_assoc($result4);

if(!empty($page_num)) {
	$row['pagenum'] = $page_num['pagenum'];
}

$stmt = $conn->prepare("SELECT * FROM locations");
$stmt->execute();
$result5 = $stmt->get_result();
$locationlist = array();
while ($location = mysqli_fetch_assoc($result5)){
	$locationlist[] = $location;
}

$stmt = $conn->prepare("
	SELECT comments.*,
	users.email, users.username FROM comments
	LEFT JOIN users ON comments.userid = users.id
	WHERE pic = ?
	ORDER BY comments.timestamp
	");
$stmt->bind_param("i", $row['id']);
$stmt->execute();

$result6 = $stmt->get_result();
$comments = array();
while ($comment = mysqli_fetch_assoc($result6)){
	$parsed = $Parsedown->text($comment['comment']);
	$comment['comment'] = $parsed;
	$comments[] = $comment;
}

$template = $twig->load('image-viewer.html');
echo $template->render(array("pic" => $row, "sitename" => $_SITENAME_, "siteurl" => $_SITE_URL_, "basepath" => $_PIC_BASE_DIR_, "thumbbasepath" => $_THUMB_DIR_, "user" => $user, "locationlist" => $locationlist, "comments" => $comments));

?>
