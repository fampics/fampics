function openModal() {
	document.getElementById('myModal').style.display = "block";
}

function closeModal() {
	document.getElementById('myModal').style.display = "none";
}

var slideIndex = 1;

// Nextprevious controls
function plusSlides(n) {
	showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
	showSlides(slideIndex = n);
}

function showSlides(n) {
	var i;
	var slides = document.getElementsByClassName("mySlides");
	//var dots = document.getElementsByClassName("demo");
	var captionText = document.getElementById("caption");
	if (n > slides.length) {slideIndex = 1}
	if (n < 1) {slideIndex = slides.length}
	for (i = 0; i < slides.length; i++) {
		slides[i].style.display = "none";
	}
	//for (i = 0; i < dots.length; i++) {
	//	dots[i].className = dots[i].className.replace(" active", "");
	//}
	slides[slideIndex-1].style.display = "block";
	//dots[slideIndex-1].className += " active";
	//captionText.innerHTML = dots[slideIndex-1].alt;
	$('#comment-form').remove();
	$('.commentlink').show();
	$('#downloadLink').attr({href: $('.image-box img')[slideIndex-1].src + "?download"});
	setSlideSize();
	setDetailsSize();
}

function setSlideSize() {
	var slide = document.getElementsByClassName("modal-content");
	var image_box = document.getElementsByClassName("image-box");
	var win_height = window.innerHeight;
	var win_width = window.innerWidth;
	var slides = document.getElementsByClassName("mySlides");
	var pic_height = slides[slideIndex-1].children[2].children[0].naturalHeight;
	var pic_width = slides[slideIndex-1].children[2].children[0].naturalWidth;
	var padding_l = window.getComputedStyle(slide[0], null).getPropertyValue('padding-left');
	var padding_r = window.getComputedStyle(slide[0], null).getPropertyValue('padding-right');
	var pic_ratio = pic_height/pic_width;

	if (pic_width < win_width) {
		new_width = pic_width + parseInt(padding_l) + parseInt(padding_r)  + "px"; 
	}

	if (pic_height > win_height) {
		new_height = (win_height + (-2 * parseInt(padding_l)) + (-50)) ;
		new_width = new_height / pic_ratio + "px";
	}

	image_box[slideIndex-1].style.width = new_width;

	// set position
	image_box[slideIndex-1].style.left = -(parseInt(new_width)/2) -200 + "px";
}

function setDetailsSize() {
	var details_box = document.getElementsByClassName('myModal-details');
	var win_height = window.innerHeight;
	var win_width = window.innerWidth;
	
	var details_height = details_box[slideIndex-1].clientHeight;
	var new_margin = ((win_height - details_height)/2) - 45;

	details_box[slideIndex-1].style.marginTop = new_margin + "px";
	
	var pic_left = document.getElementsByClassName('image-box')[slideIndex-1].style.left;
	var pic_width = document.getElementsByClassName('image-box')[slideIndex-1].style.width;
	details_box[slideIndex-1].style.left = parseInt(pic_width) + parseInt(pic_left) + "px";
}

function hideDetails() {
	var details_box = document.getElementsByClassName('myModal-details');

	for (i=0; i < details_box.length; i++) {
		details_box[i].style.width = 0;
		details_box[i].style.padding = 0;
		details_box[i].style.display = "none";
	}

	document.getElementById('hide-details').style.display = "none";
	document.getElementById('show-details').style.display = "unset";
/*
	var Slides = document.getElementsByClassName('mySlides');

	for (i=0; i < Slides.length; i++){
		var picbox = Slides[i].children[1];
		var pic_width = Slides[i].children[1].children[0].width;
		if (picbox.children[1]){
			var faces = picbox.children[1].children;
			for (j=0; j< faces.length; j++){
				var face = faces[j].children[0];
				var face_per_l = parseInt(face.style.left);
				var face_per_r = parseInt(face.style.right);
				console.log(pic_width);
                                console.log(pic_width * face_per_l);
                                console.log((pic_width + 440) * face_per_l / 100);
                                console.log(pic_width + 440);
                                console.log();
                                console.log(face);
				face.style.left  = ((pic_width + 440)*face_per_l ) / (pic_width) + "%";
				face.style.right = ((((pic_width + 420)*face_per_r )-420) / (pic_width))/10 + "%";
			}
		}
	} */
}

function showDetails() {
	var details_box = document.getElementsByClassName('myModal-details');

	for (i=0; i < details_box.length; i++) {
		details_box[i].style.width = "400px";
		details_box[i].style.padding = "20px";
		details_box[i].style.display = "block";
	}

	document.getElementById('show-details').style.display = "none";
	document.getElementById('hide-details').style.display = "unset";
/*	
	var Slides = document.getElementsByClassName('mySlides');

	for (i=0; i < Slides.length; i++){
		var picbox = Slides[i].children[1]
		var pic_width_nat = Slides[i].children[1].children[0].naturalWidth;
		var pic_width = Slides[i].children[1].children[0].width
		if (picbox.children[1]){
			var faces = picbox.children[1].children;
			for (j=0; j < faces.length; j++){
				var face = faces[j].children[0];
				var face_per_l = parseInt(face.style.left);
				var face_per_r = parseInt(face.style.right);
				console.log(pic_width);
				console.log(pic_width * face_per_l);
				console.log(pic_width * face_per_l / 100);
				console.log(pic_width + 440);
				console.log(100*((pic_width * face_per_l)/100)/(pic_width + 440));
				console.log(face);
				face.style.left = (100 * ((pic_width * face_per_l)/100) / (pic_width + 440)) + "%";
				face.style.right = (100 * (((pic_width * face_per_r)/100)+420) / (pic_width + 440)) + "%";

			}
		}
	}*/

}
