function approveUser(userID) 
{
	$.post("API/Admin/1",
		{
			method: 'approveUser',
			userID: userID
		},
		function(data, status){
			alert("data: " + data + "\nStatus: " + status);
		}
	);
	$('#approve_'+userID).hide();
}

function openRolesModal(userID, isSuperAdmin)
{
	$('#RolesBody').empty();
	rolesAvail = [
	"EDITOR",
	"MODERATOR",
	"PUBLISHER",
	"ADMIN",
	"SUPER_ADMIN"
	];
	$.post("API/Admin/2",
		{
			method: "getUserRoles",
			userID: userID
		},
		function(data, status){
			if (status == "success"){
				obj = JSON.parse(data);
				$('#RolesBody').append("<form><input type='hidden' id='userID' value='" + userID + "'></form");
				for (r in rolesAvail) {
					$('#RolesBody form').append("<div class='form-check'><input class='form-check-input' type='checkbox' name='" + rolesAvail[r] + "' id='" + rolesAvail[r] + "'><label class='form-check-label' for='" + rolesAvail[r] + "'>" + rolesAvail[r] + "</label></div>");
					if (obj.indexOf(rolesAvail[r]) > -1){
						$('#'+rolesAvail[r]).prop("checked", true);
					}
				}
				if (isSuperAdmin == "False"){
					$('#SUPER_ADMIN').prop("disabled", true);
				}
			}
			else {
				alert("data: " + data + "\nStatus: " + status);
			}
		}
	);
	
}

function submitUserRoles() 
{
	$.post("API/Admin/3",
	{
		method: "setUserRoles",
		userID: $('#userID').val(),
		editor: $('#EDITOR').prop('checked'),
		moderator: $('#MODERATOR').prop('checked'),
		publisher: $('#PUBLISHER').prop('checked'),
		admin: $('#ADMIN').prop('checked'),
		super_admin: $('#SUPER_ADMIN').prop('checked')
	},
	function(data, status){
		console.log(data)
	}
	);
}

function submitUserPassword()
{
	$.post("API/Admin/4",
		{
			method: "changeUserPassword",
			userID: $('#passwordUser').val(),
			password: $('#NewPassword').val()
		},
		function(data, status){
			console.log(data)
		}
	
	);
}

function BanUser()
{
	$.post("API/Admin/5",
		{
			method: "BanUser",
			UserID: $('#banUser').val(),
			mesg: $('#banmessage').val()
		},
		function(data, status){
			console.log(data);
		}
	);
}

function UnBanUser(UserID)
{
	$.post("API/Admin/6",
		{
			method: "UnBanUser",
			UserID: UserID
		},
		function(data, status){
			console.log(data);
		}
	);
}

function generateThumb(id)
{
	$.post("API/Admin/7",
		{
			method: "generateThumb",
			picid: id
		},
		function(data, status){
			if (status == "success")
			{
				$('#thumb_'+id).remove();
				$('#thumbcount').text($('#thumbcount').text()-1);
			}
			else {
				alert("There was an error");
			}
		}
	);
}
