function editLocation(picid) {
	$('#LocationEditModal').modal()
}

function submitLocationForm() {
	locationid = $("#LocationName").val();
	picid = $("#PicId").val();
	$.post("../API/editLocation.php",
	{
		locationid: locationid,
		pictureid: picid
	},
	function(data, status){
		if (data == "True"){
			//alert("Location updated.");
			location.reload();
		}
		else {
			alert("Data: " + data + "\nStatus: " + status);
		}
	});

}
