
function newComment(slideid) {
	$('#comments_'+slideid).after('<div id="comment-form"></div>');
	$('.commentlink').hide();
	$('#comment-form').append('<form><textarea class="form-control" id="comment-text" rows="3"></textarea><button onclick="closeCommentForm()" class="btn btn-secondary">Cancel</button><button onclick="submitCommentForm(' + slideid +')" class="btn btn-primary">Post</button></form>');
	setDetailsSize();
}

function newReply(slideid, commentid) {
	$('#comment_'+commentid).after('<div id="comment-form"></div>');
	$('.commentlink').hide();
	$('#comment-form').append('<form><textarea class="form-control" id="comment-text" rows="3"></textarea><button onclick="closeCommentForm()" class="btn btn-secondary">Cancel</button><button onclick="submitCommentForm(' + slideid + ',' + commentid +')" class="btn btn-primary">Post</button></form>');
}

function closeCommentForm() {
	$('#comment-form').remove();
	$('.commentlink').show();
	setDetailsSize();
}

function submitCommentForm(slideid, commentid=0) {
	commentText = $('#comment-text').val();
	siteurl = $('#siteurl').text()
	$.post(siteurl+"API/addcomment.php",
		{
			pic: slideid,
			comment: commentText,
			commentid: commentid
		},
		function(data, status){
			if (commentid != 0){
				$('#comment_'+commentid).append(data.parsed);
			}
			else {
				$('#comments_'+slideid).append(data.parsed);
			}
		}
	);
}
