<?php

include '../src/common.inc';
include '../src/database.php';

if (!$auth->isLoggedIn()) {
	header('Location: '.$_SITE_URL_.'login?goto=admin');
}

if (!canOpenAdminPanel($auth)) {
	header('Location: '.$_SITE_URL_);
}

$db = new Database();

$userlist = $db->getUsers();
for ($i = 0; $i < count($userlist); $i++){
	$metas = $db->getUserMetas($userlist[$i]['id']);
	if (!empty($metas)) {
		if (!empty($metas['UserApproved'])){
			$userlist[$i]['approved'] = true;
		}
		if (!empty($metas['UserBanned'])) {
			$userlist[$i]['banned'] = true;
		}
		$userlist[$i]['metas'] = $metas;
	}
	$userlist[$i]['roles'] = $auth->admin()->getRolesForUserById($userlist[$i]['id']);
}

$thumblessPics = $db->getThumblessPics();

$template = $twig->load('admin-panel.html');
echo $template->render(array("sitename" => $_SITENAME_, "siteurl" => $_SITE_URL_, "user" => $user, "userlist" => $userlist, "thumblessPics" => $thumblessPics));
?>
