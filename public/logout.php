<?php

include '../src/common.inc';

$db = new \Delight\Db\PdoDsn('mysql:dbname='.$_DATABASE_NAME_.";host=".$_DATABASE_SERVER_.";charset=utf8mb4", $_DATABASE_USER_, $_DATABASE_PASS_);
$auth = new \Delight\Auth\Auth($db);

try {
	$auth->logOutEverywhere();
	header('Location: '.$_SITE_URL_);
}
catch (\Delight\Auth\NotLoggedInException $e) {
	echo "Not logged in";
}

?>
