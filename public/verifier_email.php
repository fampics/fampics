<?php

include '../src/common.inc';

try {
	$auth->confirmEmail($_GET['selector'], $_GET['token']);
	$message = array("text" => "Your email address has been confirmed.", "state" => "valid");
}
catch (\Delight\Auth\InvalidSelectorTokenPairException $e) {
	//invalid token
	$message = array("text" => "Your token is invalid", "state" => "invalid");
 }
catch (\Delight\Auth\TokenExpiredException $e) {
	// token expired
	$message = array("text" => "The token has expired.", "state" => "invalid");
}
catch (\Delight\Auth\UserAlreadyExistsException $e) {
	// email address already exists
	$message = array("text" => "Sorry, but that email is already used.", "state" => "invalid");
}
catch (\Delight\Auth\TooManyRequestsException $e) {
	// too many requests
	$message = array("text" => "Sorry, but you have made too many requests.", "state" => "invalid");
}

$template = $twig->load('email-verify.html');
echo $template->render(array("sitename" => $_SITENAME_, "siteurl" => $_SITE_URL_, "user" => $user, "message" => $message));

?>
