<?php
include '../src/common.inc';

if (!$auth->isLoggedIn() and $_LOGIN_REQUIRED_) {
	header('Location: '.$_SITE_URL_.'login?goto=tags/'.$_GET['tag']);
}

$stmt = $conn->prepare("
	SELECT tags.name, picture.*, thumbs.thumbpath FROM tags
	LEFT JOIN tag_instance ON tags.id = tag_instance.tagid
	LEFT JOIN picture ON tag_instance.pic = picture.id
	LEFT JOIN thumbs ON picture.thumbid = thumbs.id
	WHERE tags.name = ?
	");
$stmt->bind_param('s', $_GET['tag']);
$stmt->execute();
$result = $stmt->get_result();

$rows = array();

while($row = mysqli_fetch_assoc($result)) {
	$rows[] = $row;
}
$template = $twig->load('tag-details.html');
echo $template->render(array("tag" => $rows[0], "sitename" => $_SITENAME_, "siteurl" => $_SITE_URL_, "basepath" => $_PIC_BASE_DIR_, "thumbbasepath" => $_THUMB_DIR_, "pics" => $rows, "user" => $user));
?>
