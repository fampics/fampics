<?php
include '../src/common.inc';

if (!$auth->isLoggedIn() and $_LOGIN_REQUIRED_) {
	header('Location: '.$_SITE_URL.'login?goto=tags');
}

$stmt = $conn->prepare("
	SELECT COUNT(tag_instance.tagid) AS tagcount, tags.name, tags.person
	FROM tag_instance
	LEFT JOIN tags ON tag_instance.tagid = tags.id
	GROUP BY tagid
	ORDER BY tagcount DESC
	");
$stmt->execute();
$result = $stmt->get_result();

$rows = array();

while ($row = mysqli_fetch_assoc($result)) {
	$rows[] = $row;
}
$template = $twig->load('tags.html');
echo $template->render(array("sitename" => $_SITENAME_, "siteurl" => $_SITE_URL_, "basepath" => $_PIC_BASE_DIR_, "thumbbasepath" => $_THUMB_DIR_, "tags" => $rows, "user" => $user));
?>
