## Next release

## 0.9 Security/Refactor
~v0.9
July 25 2018

## Features:
* Added MIT License.
* Added the first unit tests.
* Restructured the project.
* Converted the modal viewer into a macro to allow code reuse.
* A bunch of smaller things to make it more secure/easier to work on in the future.

### Bugs:
* Fixed a bug that caused the notifications indicator to only appear on some pages. #140
* Fixed a bug that caused an error message to appear if the user was not logged in. #143
* Fixed an error that was given because code was called to early in myModal.js


## 0.7 Comments
~v0.7
July 13 2018

* Comments can now be listed along with a picture.
* Comments can now be written if logged in from all 3 views.
* Fampics markdown now supports tagging users and locations
* SubComments can be displayed and written
* users can subscribe to a thread of comments, and receive notifications when new messages are added.
* When a user gets a notification a badge appears on the notifications menu. This badge disappears when the messages have been read.

## 0.6 Locations
~v0.6
July 5 2018
* Added a locations to the image viewers
* added a list of locations
* Added a list galleray of images at each location
* users with the role of editor can now edit/add an images location


## 0.4 Person Tags
~v0.4
June 18 2018
* Added people tags to all viewers
* Added a list of all people, and one for eveyone with the same last name
* Added a bio for people, which supports markdown.
* converted the generic viewer to use the custom lighbox system.

## 0.3 Tags
~v0.3
June 14 2018
* Implimented a new custom lightbox viewer
* Added a new page that lists all the tags
* Added a tag details page, that lists the pictures taged with each tag
* List the tags an image is tagged with in all the viewers

## 0.2 Proper Viewers
~v0.2
June 11 2018
* Caption and title display in lightbox
* Thumbnails are now used, a default thumbnail will be used if one is not set
* Album view created
 * Album view shows album pages,where individual images can be clicked on and a lightbox will open the image.
 * Title Tooltips for the clickable areas

## 0.1 The Bare Minimum
~v0.1
June 9 2018
* Initial release
* Generic viewer implemented
* Templates
* Config file
