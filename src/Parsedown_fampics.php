<?php

require __DIR__ . '/../vendor/autoload.php';

class Parsedown_fampics extends Parsedown
{
    function __construct()
    {
        $this->InlineTypes['@'][] = 'Mention';
        $this->inlineMarkerList .= '@';
    }

    protected function inlineMention($excerpt)
    {
        // match for people tags
        if (preg_match('/\B@([a-zA-Z][\w-]+)\.([a-zA-Z][\w-]+)/', $excerpt['text'], $matches)) {
            return array(
                'extent' => strlen($matches[0]),
                'element' => [
                    'name' => 'a',
                    'text' => $matches[1] . " " . $matches[2],
                    'attributes' => [
                        'href' => $GLOBALS['_SITE_URL_'] . 'people/' . $matches[2] . '/' . $matches[1],
                        'class' => 'mention mention-person'
                    ]
                ]
            );
        }
        // match for user tags
        elseif (preg_match('/\B@([a-zA-Z][\w-]+)/', $excerpt['text'], $matches)) {
            return array(
                'extent' => strlen($matches[0]),
                'element' => [
                    'name' => 'a',
                    'text' => $matches[1] ,
                    'attributes' => [
                        'href' => $GLOBALS['_SITE_URL_'] . 'users/' . $matches[1] ,
                        'class' => 'mention mention-user'
                    ]
                ]
            );
        }
        //match for a location with two layers of parents
        elseif (preg_match('/\B@@\((.+)\)\.\((.+)\)\.\((.+)\)/', $excerpt['text'], $matches)) {
            return array(
                'extent' => strlen($matches[0]),
                'element' => [
                    'name' => 'a',
                    'text' => $matches[1],
                    'attributes' => [
                        'href' => $GLOBALS['_SITE_URL_'] . 'location/' . $matches[3] . "/" . $matches[2] . "/" . $matches[1],
                        'class' => 'mention mention-location'
                    ]
                ]
            );
        }
        // match for a location with one parent
        elseif (preg_match('/\B@@\((.+)\)\.\((.+)\)/', $excerpt['text'], $matches)) {
            return array(
                'extent' => strlen($matches[0]),
                'element' => [
                    'name' => 'a',
                    'text' => $matches[1],
                    'attributes' => [
                        'href' => $GLOBALS['_SITE_URL_'] . 'location/' . $matches[2] . "/" . $matches[1],
                        'class' => 'mention mention-location'
                    ]
                ]
            );
        }
        // match for a location with no parents
        elseif (preg_match('/\B@@\((.+)\)/', $excerpt['text'], $matches)) {
            return array(
                'extent' => strlen($matches[0]),
                'element' => [
                    'name' => 'a',
                    'text' => $matches[1],
                    'attributes' => [
                        'href' => $GLOBALS['_SITE_URL_'] . 'location/' . $matches[1],
                        'class' => 'mention mention-location'
                    ]
                ]
            );
        }
    }
}
