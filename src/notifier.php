<?php
function SendNotification($userDetails, $title, $message)
{
    $mail = $GLOBALS['mail'];
    if ($userDetails['email'] == '' | $userDetails['email'] == null) {
        return false;
    }
    try {
        $mail->addAddress($userDetails['email']);
        $mail->isHTML(true);
        $mail->Subject = $title;
        $mail->Body = $message;
        $mail->send();
    } catch (Exception $e) {
        echo $mail->ErrorInfo;
        return false;
    }
    return true;
}

function AddSiteNotification($UserTagged, $message, $from, $type, $conn, $url)
{

    $GLOBALS['debug'][] = "Site notification function";
    $sql  = "INSERT INTO notifications ";
    $sql .= "(id, user, timestamp, message, fromUser, type, url, beenread) ";
    $sql .= "VALUES (NULL, '" . $UserTagged['id'] . "', CURRENT_TIMESTAMP, '" . $message . "', '" . $from . "', '" . $type . "', '" . $url . "', 0)";
    $stmt = $conn->prepare("INSERT INTO notifications (id, user, timestamp, message, fromUser, type, url, beenread) VALUES (null, '?', CURRENT_TIMESTAMP, '?', '?', '?', '?', 0)");
    $stmt->bind_param("isiss", $userTagged['id'], $message, $from, $type, $url);
    $stmt->execute();
    $result = $stmt->get_result();
    if ($result->affected_rows > 0) {
        // sucess
        return true;
    } else {
        // Fail
        return false;
    }
}

function notificationUserTagged($taggedUser, $img, $from, $comment, $conn, $url)
{
    
    $stmt = $conn->prepare("SELECT * FROM users WHERE username = '?'");
    $stmt->bind_param('i', $taggedUser);
    $stmt->execute();
    $result = $stmt->get_result();
    
    if (mysqli_num_rows($result) > 0) {
        $UserDetails = mysqli_fetch_assoc($result);
        // Email
        $template = $GLOBALS['twig']->load('emails/userTaggedInComment.html');
        $message = $template->render(array("GLOBALS" => $GLOBALS, "from" => $from));
        SendNotification($UserDetails, "You have been tagged in a comment on " . $GLOBALS['_SITENAME_'], $message);

        // site notification
        
        $template = $GLOBALS['twig']->load('notifications/userTaggedInComment.html');
        $message = $template->render(array("GLOBALS" => $GLOBALS, "from" => $from, "pic" => $img));
        AddSiteNotification($UserDetails, $message, $from, "Tag", $conn, $url);
    }
    //print_r($taggedUser);
    //print_r($img);
    //print_r($from);
    //print_r($comment);
}

function scanComment($comment, $pic, $auth, $conn, $parent)
{

    $url = $GLOBALS['_SITE_URL_'] . "image/" . $pic ;
    // Run Regex on the comment looking for tags.
    preg_match_all('/\B@([a-zA-Z][\w-]+)\.([a-zA-Z][\w-]+)/', $comment, $matches_people);
    preg_match_all('/\B@([a-zA-Z][\w-]+)\s/', $comment, $matches_users);

    for ($i = 0; $i < count($matches_people[0]); $i++) {
        // do something with people tags.
        
        //echo "\nperson: ";
        //print_r($matches_people[0][$i]);
    }

    for ($i = 0; $i < count($matches_users[0]); $i++) {
        // For each user tagged.

        $username = $matches_users[1][$i];
        $GLOBALS['debug'][] = "Matched user";
        notificationUserTagged($username, $pic, $auth->getUserId(), $comment, $conn, $url);
    }

    //
    //Check if anyone is following the comments
    //
    
    if ($parent != null) {
        $stmt = $conn->prepare("SELECT * FROM comment_subscriptions WHERE comment = ?");
        $stmt->bind_param('i', $parent);
        $stmt->execute();
        $result = $stmt->get_result();
        while ($ref = mysqli_fetch_assoc($result)) {
            $stmt = $conn->prepare("SELECT * FROM users WHERE id = ?");
            $stmt->bind_param('i', $ref['user']);
            $stmt->execute();
            $result2 = $stmt->get_results();
            $UserDetails = mysqli_fetch_assoc($result2);
            
            $template = $GLOBALS['twig']->load('emails/SubscriptionNewComment.html');
            $message = $template->render(array("GLOBALS" => $GLOBALS));
            SendNotification($UserDetails, "A comment thread you are following was added to - ".$GLOBALS['_SITENAME_'], $message);
            
            $template = $GLOBALS['twig']->load('notifications/SubscriptionNewComment.html');
            $message = $template->render(array("GLOBALS" => $GLOBALS));
            AddSiteNotification($UserDetails, $message, $auth->getUserId(), "Subscript", $conn, $url);
        }
    }
}
