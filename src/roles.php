<?php

final class MyRoles
{
    const USER = \Delight\Auth\Role::CONSUMER;

    private function __constuct()
    {
    }
}

function canOpenAdminPanel(\Delight\Auth\Auth $auth)
{
    return $auth->hasAnyRole(
        \Delight\Auth\Role::ADMIN,
        \Delight\Auth\Role::SUPER_ADMIN
    );
}

function canOpenEditorPanel(\Delight\Auth\Auth $auth)
{
    return $auth->hasAnyRole(
        \Delight\Auth\Role::EDITOR,
        \Delight\Auth\Role::ADMIN,
        \Delight\Auth\Role::SUPER_ADMIN
    );
}

function canEditImageLocation(\Delight\Auth\Auth $auth)
{
    return $auth->hasAnyRole(
        \Delight\Auth\Role::EDITOR,
        \Delight\Auth\Role::ADMIN,
        \Delight\Auth\Role::SUPER_ADMIN
    );
}

function canAddImageLocation(\Delight\Auth\Auth $auth)
{
    return $auth->hasAnyRole(
        \Delight\Auth\Role::EDITOR,
        \Delight\Auth\Role::ADMIN,
        \Delight\Auth\Role::SUPER_ADMIN
    );
}
