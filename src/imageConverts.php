<?php

use Orbitale\Component\ImageMagick\Command;

class imageConverter
{
    private $command;
    public function __construct()
    {
        $this->command = new Command();
    }

    public function generateThumb($input, $output) : Object
    {
        $response = $this->command

            ->convert($input)
            ->resize('250x250')
            ->file($output, false)
            ->run();
        return $response;
    }
}
