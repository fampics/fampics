<?php


class Database
{
    private $conn;

    public function __construct()
    {
        $this->conn = new mysqli($GLOBALS['_DATABASE_SERVER_'], $GLOBALS['_DATABASE_USER_'], $GLOBALS['_DATABASE_PASS_'], $GLOBALS['_DATABASE_NAME_']);
        if ($this->conn->connect_error) {
            die("Conection Failed: " . $conn->connect_error);
        }
        return true;
    }

    public function getUserMetas($userID) : array
    {
        $stmt = $this->conn->prepare("SELECT * FROM users_meta WHERE userid = ?");
        $stmt->bind_param('i', $userID);
        $stmt->execute();
        $result = $stmt->get_result();

        $rows = array();
        while ($row = $result->fetch_assoc()) {
            $rows[$row['tag']] = $row;
        }
        return $rows;
    }

    public function getUsers() : array
    {
        $stmt = $this->conn->prepare("SELECT * FROM users");
        $stmt->execute();
        $result = $stmt->get_result();

        $rows = array();
        while ($row = $result->fetch_assoc()) {
            $rows[] = $row;
        }
        return $rows;
    }

    public function getThumblessPics() : array
    {
        $stmt = $this->conn->prepare("SELECT * FROM picture WHERE thumbid = 0 AND pagenum IS NULL");
        $stmt->execute();
        $result = $stmt->get_result();

        $pics = array();
        while ($pic = $result->fetch_assoc()) {
            $pics[] = $pic;
        }
        return $pics;
    }
    public function getPicDetailsFromId($id) : array
    {
        $stmt = $this->conn->prepare("SELECT * FROM picture WHERE id = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_assoc();
    }

    public function addThumb($thumbpath, $picid) : Bool
    {
        $stmt = $this->conn->prepare("INSERT INTO thumbs (id, thumbpath) VALUES (NULL, ?)");
        $stmt->bind_param('s', $thumbpath);
        $stmt->execute();
        $thumbid = $this->conn->insert_id;
        $stmt->close();
        $stmt = $this->conn->prepare("UPDATE picture SET thumbid = ? WHERE picture.id = ?");
        $stmt->bind_param('ii', $thumbid, $picid);
        $stmt->execute();
        return true;
    }
}
