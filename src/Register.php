<?php

class MissingUserNameException extends Exception
{

}
class MissingEmailException extends Exception
{

}
class MissingPasswordException extends Exception
{

}

final class Register
{
    private $username;
    private $email;
    private $password;

    private function __construct(string $username, string $email, string $password)
    {
        $this->username = $username;
        $this->email = $email;
        $this->password = $password;
    }

    public static function registration(string $username, string $email, string $password): self
    {
        if (!isset($username) | $username == '' | $username == null) {
            throw new MissingUserNameException();
        }
        if (!isset($email) | $email == '' | $email == null) {
            throw new MissingEmailException();
        }
        if (!isset($password) | $password == '' | $password == null) {
            throw new MissingPasswordException();
        }
        return new self($username, $email, $password);
    }

    public function submit($auth, $url): void
    {
        $userId = $auth->registerWithUniqueUsername($this->email, $this->password, $this->username, function ($selector, $token) use (&$url) {
            $url .= 'verify_email?elector=' . urlencode($selector) . "&token=" . urlencode($token);
        });
        $this->url = $url;
    }
}
