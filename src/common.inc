<?php

include __DIR__ . '/../config/config.inc';
include 'roles.php';
require __DIR__ . '/../vendor/autoload.php';

// setup emails
use PHPMailer\PHPMailer\PHPMailer;

$mail = new PHPMailer;

$mail->isSMTP();
$mail->SMTPDebug = 0; // set this to 2 for full debugging, 0 for production
$mail->Host = 'localhost';
$mail->Port = 1025;
$mail->setFrom('No-Reply@example.com', $_SITENAME_);

// Auth setup

$db = new \Delight\Db\PdoDsn('mysql:dbname='.$_DATABASE_NAME_.';host='.$_DATABASE_SERVER_.';charset=utf8mb4', $_DATABASE_USER_, $_DATABASE_PASS_);
$auth = new \Delight\Auth\Auth($db);

$user = array();

if ($auth->isLoggedIn()) {
    $user['logged_in'] = true;
    $user['username'] = $auth->getUsername();
    $user['AuthRoles'] = $auth->getRoles();
    $user['email'] = $auth->getEmail();
    $user['id'] = $auth->getUserId();
    $user['roles'] = array(
        "admin_panel" => canOpenAdminPanel($auth),
        "editor_panel"=> canOpenEditorPanel($auth),
        "canEditImageLocation" => canEditImageLocation($auth),
        "canAddImageLocation" => canAddImageLocation($auth),
        "isSuperAdmin" => $auth->hasRole(\Delight\Auth\Role::SUPER_ADMIN)
    );
}


// DB common setup

$conn = new mysqli($_DATABASE_SERVER_, $_DATABASE_USER_, $_DATABASE_PASS_, $_DATABASE_NAME_);

if ($conn->connect_error) {
    die("conection Failed: " . $conn->connect_error);
}

// Get users notifications

$notifications = array();
if (($user)) {
    $stmt = $conn->prepare("SELECT * FROM notifications WHERE user=?");
    $stmt->bind_param('i', $user['id']);
    $stmt->execute();
    $result = $stmt->get_result();
    if (mysqli_num_rows($result) > 0) {
        while ($notif = mysqli_fetch_assoc($result)) {
            $notifications[] = $notif;
        }
    }
}

// Templates setup


if (!isset($TEMPLATE_DISABLE)) {
    $loader = new Twig_Loader_Filesystem('../resources/templates');
    $twig = new Twig_Environment($loader, array(
        'debug' => true
    ));

    $twig->addExtension(new Twig_Extension_Debug());
    $twig->addGlobal("notifications", $notifications);
}
if (isset($TEMPLATE_ALTERNATE)) {
    $loader = new Twig_Loader_Filesystem($TEMPLATE_ALTERNATE);
    $twig = new Twig_Environment($loader, array(
        'debug' => true
    ));

    $twig->addExtension(new Twig_Extension_Debug());
    $twig->addGlobal("notifications", $notifications);
}
