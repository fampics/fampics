<?php

include_once __DIR__ . '/../src/database.php';

final class Login
{
    public $message;
    private $username;
    private $email;
    private $password;
    private $rememberDuration;
    private $referer;
    private $db;

    private function __construct(string $email, string $password, $rememberDuration, string $referer)
    {
        $this->email = $email;
        $this->password = $password;
        $this->rememberDuration= $rememberDuration;
        $this->referer = $referer;
        $this->db = new Database();
    }

    private function userApproved($userID) : bool
    {
        if (isset($GLOBALS['_SETTINGS_']['requireAccountApproval'])) {
            $metas = $this->db->getUserMetas($userID);
            if (isset($metas['UserApproved']) && $metas['UserApproved']['value'] == "true") {
                return true;
            } else {
                $this->message = array("text" => "This user needs to be approved be an admin.", "state" => "invalid");
                return false;
            }
        } else {
            return true;
        }
    }

    private function userBanned($userID) : bool
    {
        $metas = $this->db->getUserMetas($userID);
        if (isset($metas['UserBanned'])) {
            $this->message = array("text" => $metas['UserBanned']['value'], "state" => "invalid");
            return true;
        } else {
            return false;
        }
    }

    private function isUserCleared($userID) : bool
    {
        if ($this->userApproved($userID) && !$this->userBanned($userID)) {
            return true;
        } else {
            return false;
        }
    }

    public function setup(string $email, string $password, $keepalive, string $referer): self
    {
        if (isset($keepalive) | $keepalive == 1 | $keepalive == true) {
            $rememberDuration = (int) (60*60*24*365.25);
        } else {
            $rememberDuration = null;
        }
        return new self($email, $password, $rememberDuration, $referer);
    }

    public function login($auth)
    {
        try {
            $auth->login($this->email, $this->password, $this->rememberDuration);
        } catch (\Delight\Auth\InvalidEmailException $e) {
            return array("text" => "Email address Invalid", "state" => "invalid", "email" => $this->email);
        } catch (\Delight\Auth\InvalidPasswordException $e) {
            return array("text" => "Password Incorrenct", "state" => "invalid");
        } catch (\Delight\Auth\EmailNotVerifiedException $e) {
            return array("text" => "Email Address not verified. Plaese Check your Email.", "state" => "invalid", "email" => $this->email);
        } catch (\Delight\Auth\TooManyRequestsException $e) {
            return array("text" => "Too many requests", "state" => "invalid");
        }
        if ($this->isUserCleared($auth->id())) {
            header("Location: " . $_SITE_URL_ . $this->referer);
        } else {
            $auth->logOut();
            return $this->message;
        }
    }
}
