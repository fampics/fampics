<?php

use \PHPUnit\Framework\Testcase;

/*
 * @requires extension mysqli
 */

final class LoginTest extends Testcase
{
	public function testCanCreateLoginObject(): void
	{
		if (extension_loaded('mysqli'))
		{
		$this->assertInstanceOf(
			Login::class,
			Login::setup('admin@fampics.com', 'password', null, '.')
		);
		$this->assertInstanceOf(
			Login::class,
			Login::setup('admin@fampics.com', 'password', true, '.')
		);
		}
		else {
			$this->markTestSkipped('The MYSQLi extension is not available');
		}
	}
}
?>
