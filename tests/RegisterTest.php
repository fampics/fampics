<?php

use \PHPUnit\Framework\TestCase;

final class RegisterTest extends TestCase
{
	public function testCanCreateAccountWithValidDetails(): void
	{
		$this->assertInstanceOf(
			Register::class,
			Register::registration('fampics','fampics@example.com','password')
		);
	}
	public function testCannotCreateAccountWithoutUsername(): void
	{
		$this->expectException(MissingUsernameException::class);
		Register::registration('','fampics@example.com','password');
	}
	public  function testCannotCreateAccountWithoutEmail(): void
	{
		$this->expectException(MissingEmailException::class);
		Register::registration('fampics','','password');
	}
	public function testCannotCreateAccountWithoutPassword(): void
	{
		$this->expectException(MissingPasswordException::class);
		Register::registration('fampics','fampics@example.com','');
	} 
}
?>
