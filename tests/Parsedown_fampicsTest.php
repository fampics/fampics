<?php

use \PHPUnit\Framework\TestCase;

final class Parsedown_fampicsTest extends TestCase
{
	public function testCanCreateInstanceOfParser(): void
	{
		$this->assertInstanceOf(
			Parsedown_fampics::class,
			new Parsedown_fampics
		);
	}

	public function testCanTagAPersonInMiddleOfString(): void
	{
		$Parsedown = new Parsedown_fampics();
		$this->assertEquals(
			$Parsedown->text("Hello @First.Last, How are you?"),
			'<p>Hello <a href="http://fampics.com/people/Last/First" class="mention mention-person">First Last</a>, How are you?</p>'
		);
	}

	public function testCanTagPersonAlone(): void
	{
		$Parsedown = new Parsedown_fampics();
		$this->assertEquals(
			$Parsedown->text("@First.Last"),
			'<p><a href="http://fampics.com/people/Last/First" class="mention mention-person">First Last</a></p>'
		);
		$this->assertEquals(
			$Parsedown->text(" @First.Last"),
			'<p><a href="http://fampics.com/people/Last/First" class="mention mention-person">First Last</a></p>'
		);
	}
	
	public function testCanTagUserInMiddleOfString(): void
	{
		$Parsedown = new Parsedown_fampics();
		$this->assertEquals(
			$Parsedown->text("Hello @username, I tagged you"),
			'<p>Hello <a href="http://fampics.com/users/username" class="mention mention-user">username</a>, I tagged you</p>'
		);
	}

	public function testCanTagLocationWithTwoParents(): void
	{
		$Parsedown = new Parsedown_fampics();
		$this->assertEquals(
			$Parsedown->text('@@(Location).(Parent).(Grandparent)'),
			'<p><a href="http://fampics.com/location/Grandparent/Parent/Location" class="mention mention-location">Location</a></p>'
		);
	}

	public function testCanTagLocationWithOneParent(): void
	{
		$Parsedown = new Parsedown_fampics();
		$this->assertEquals(
			$Parsedown->text('@@(Location).(Parent)'),
			'<p><a href="http://fampics.com/location/Parent/Location" class="mention mention-location">Location</a></p>'
		);
	}

	public function testCanTagLocationWithNoParents(): void
	{
		$Parsedown = new Parsedown_fampics();
		$this->assertEquals(
			$Parsedown->text('@@(Location)'),
			'<p><a href="http://fampics.com/location/Location" class="mention mention-location">Location</a></p>'
		);
	}

	public function testParseWithBokenTag(): void
	{
		$Parsedown = new Parsedown_fampics();
		$this->assertEquals(
			$Parsedown->text('@'),
			'<p>@</p>'
		);
	}
}
?>
