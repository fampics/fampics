<?php

global $_SITE_URL_;
global $_DATABASE_SERVER_;
global $_DATABASE_USER_;
global $_DATABASE_PASS_;
global $_DATABASE_NAME_;
$_SITE_URL_ = "http://fampics.com/";
$_DATABASE_SERVER_ = getenv("FAMPICS_DB_SERVER");
$_DATABASE_USER_ = getenv("FAMPICS_DB_USER");
$_DATABASE_PASS_ = getenv("FAMPICS_DB_PASS");
$_DATABASE_NAME_ = getenv("FAMPICS_DB_NAME");
?>
