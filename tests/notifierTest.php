<?php

use \PHPUnit\Framework\Testcase;
use \PHPMailer\PHPMailer\PHPMailer;
final class notifierTest extends TestCase
{

	protected function setUp()
	{
		global $mail;
		$mail = new PHPMailer;
		$mail->isSMTP();
		$mail->Host = 'localhost';
		$mail->Port = 1025;
		$mail->setFrom('No-Reply@fampics.com','fampics.com');
	}

	public function testCanSendEmail(): void
	{
		$userDetails['email'] = "test@fampics.com";
		$title = "testCanSendMail";
		$message = "Email from unit testing";

		$this->assertTrue(SendNotification($userDetails,$title,$message));
	}

	public function testCanNotSendEmailWithNoAddress(): void
	{
		$userDetails['email'] = '';
		$title = "testCanNotSendEmailWithNoAddress";
		$message = "Email from unit testing";

		$this->assertFalse(SendNotification($userDetails,$title,$message));
	}
}
?>
