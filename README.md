Fampics is a webapp for shareing family pictures. It was written specifically for older scanned pictures/slides to be shared with family.

Features:

* Image viewers that support collections and albums.
* Album View where you can share the pages of an album as they exist in the phsical album.
* User account and the option to require a login to view the site.
* Tags can be attached to image, and all the images with one tag can be viewed from the tag page.
* People can be tagged in the images, and clicking on these tags will bring you to a person page.
* A page to show all the image that a person is tagged in.
* Person Pages can have a biography on the person that supports markdown.
* Markdown suports tagging users, people, and locations.
* Images can have locations attached to them.
* Location Paged that show all the images at that location, plus a map of the location.
* Comments on images, these support the full markup(Including tags) used thoughout the site.
* Users can reply to Comments.
* Users will recieve a notification on the site, and an email when they are tagged in a comment.
* Users can be subscribed to a comment, and will recieve notifications when it is replyed to. (NOTE: This subscription currently needs to be added directly in the database)
* WIP: User Profile pictures(Currently everyone has the same one).
*

Admin/Editor Features:
* Editors can change the location that is attached to an image.
*

Future Features:

* New user accounts requiring approval.
* Editors can add locations
* Editors can change the title and description of an image.
* Editors can change the album of an image.
* Editors can upload images.
* Editors can add/remove tags for an image.
* Editors can create a new person.
* Editors can tag people in images.
* Editors can remove people tags.
* Editors can remove people.
* Admin can link a person to a user.
* Admin can add roles to a user.
* Admin can remove roles from a user.
* Admin can block a user from the site(I doubt there is a huge need for this).
* 